## [4.12.1](https://gitlab.com/to-be-continuous/golang/compare/4.12.0...4.12.1) (2025-01-31)


### Bug Fixes

* **sbom:** only generate SBOMs on prod branches, integ branches and release tags ([261e8ce](https://gitlab.com/to-be-continuous/golang/commit/261e8ceb2d360768de0cc8fe3326cc9dfea4c121))

# [4.12.0](https://gitlab.com/to-be-continuous/golang/compare/4.11.1...4.12.0) (2025-01-27)


### Features

* disable tracking service by default ([12a9a9b](https://gitlab.com/to-be-continuous/golang/commit/12a9a9b89203b5da4d3ed4b78e7972c516a994c0))

## [4.11.1](https://gitlab.com/to-be-continuous/golang/compare/4.11.0...4.11.1) (2024-12-06)


### Bug Fixes

* semgrep subdir ([0e26288](https://gitlab.com/to-be-continuous/golang/commit/0e26288dd6b27ce3e4b92ab5c21e6a73d1152902))

# [4.11.0](https://gitlab.com/to-be-continuous/golang/compare/4.10.0...4.11.0) (2024-08-30)


### Features

* standard TBC secrets decoding ([89e0f0f](https://gitlab.com/to-be-continuous/golang/commit/89e0f0fb93c2e1a21abcd09977651b567f181ed1))

# [4.10.0](https://gitlab.com/to-be-continuous/golang/compare/4.9.2...4.10.0) (2024-07-05)


### Features

* optional installation of Go tools ([5a78d9d](https://gitlab.com/to-be-continuous/golang/commit/5a78d9d8000b45edf165940eba35b6f196e97bf5))

## [4.9.2](https://gitlab.com/to-be-continuous/golang/compare/4.9.1...4.9.2) (2024-07-02)


### Bug Fixes

* go-build rule when build & test are run separately ([9c27782](https://gitlab.com/to-be-continuous/golang/commit/9c2778281bb93a12c3376cb128f8c53e9860b826))

## [4.9.1](https://gitlab.com/to-be-continuous/golang/compare/4.9.0...4.9.1) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([de9419c](https://gitlab.com/to-be-continuous/golang/commit/de9419ce66b9dcbc10a8cdcb6b15e52f8aae43e2))

# [4.9.0](https://gitlab.com/to-be-continuous/golang/compare/4.8.3...4.9.0) (2024-04-09)


### Features

* add go generate support ([3f24f7b](https://gitlab.com/to-be-continuous/golang/commit/3f24f7b5c88aa2a70a75f407e60b0f2b35cdf98e))

## [4.8.3](https://gitlab.com/to-be-continuous/golang/compare/4.8.2...4.8.3) (2024-2-21)


### Bug Fixes

* go-test job failure when go_text_report file is too big ([66a4de0](https://gitlab.com/to-be-continuous/golang/commit/66a4de0d6f48f0a73d496dc44f13d0050f29e971))

## [4.8.2](https://gitlab.com/to-be-continuous/golang/compare/4.8.1...4.8.2) (2024-1-29)


### Bug Fixes

* check emptyness of GO_TEST_IMAGE ([0df5095](https://gitlab.com/to-be-continuous/golang/commit/0df50956332d137492b8540cb6f9a8847892e74a)), closes [#34](https://gitlab.com/to-be-continuous/golang/issues/34)

## [4.8.1](https://gitlab.com/to-be-continuous/golang/compare/4.8.0...4.8.1) (2024-1-28)


### Bug Fixes

* GO_BUILD_PACKAGES and GO_VULNCHECK_ARGS input mapping ([0dcfab6](https://gitlab.com/to-be-continuous/golang/commit/0dcfab68ac456a925e839f05abbda825529d5aa5))

# [4.8.0](https://gitlab.com/to-be-continuous/golang/compare/4.7.0...4.8.0) (2024-1-27)


### Features

* migrate to CI/CD component ([9055ab5](https://gitlab.com/to-be-continuous/golang/commit/9055ab51647d59e7621edf97952d2ab5ad0476a7))

# [4.7.0](https://gitlab.com/to-be-continuous/golang/compare/4.6.1...4.7.0) (2023-12-8)


### Features

* use centralized tracking image (gitlab.com) ([c8eda0d](https://gitlab.com/to-be-continuous/golang/commit/c8eda0dc8b49baaed171da70cef60715358f2ffe))

## [4.6.1](https://gitlab.com/to-be-continuous/golang/compare/4.6.0...4.6.1) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([2a359ba](https://gitlab.com/to-be-continuous/golang/commit/2a359ba1c4c4558d94d4d6152e2c828f3e5a3789))

# [4.6.0](https://gitlab.com/to-be-continuous/golang/compare/4.5.1...4.6.0) (2023-10-11)


### Features

* add `GO_COBERTURA_FLAGS` env var for `gocover-cobertura` ([d929e79](https://gitlab.com/to-be-continuous/golang/commit/d929e79b221c7298387f764e94de1531b8be7c65))

## [4.5.1](https://gitlab.com/to-be-continuous/golang/compare/4.5.0...4.5.1) (2023-08-11)


### Bug Fixes

* **sbom:** manage separate GitLab cache to prevent permission denied error ([83aadd8](https://gitlab.com/to-be-continuous/golang/commit/83aadd8b8eb84ac0055847aa787a94f31a117ee9)), closes [#26](https://gitlab.com/to-be-continuous/golang/issues/26)

# [4.5.0](https://gitlab.com/to-be-continuous/golang/compare/4.4.0...4.5.0) (2023-08-07)


### Features

* switch to current Debian stable flavor image ([8473607](https://gitlab.com/to-be-continuous/golang/commit/847360702da5c3f88e256291b86bd0879db170d3))

# [4.4.0](https://gitlab.com/to-be-continuous/golang/compare/4.3.0...4.4.0) (2023-06-27)


### Features

* reuse $GO_BUILD_MODE for Sbom ([4d90e5c](https://gitlab.com/to-be-continuous/golang/commit/4d90e5cd834e0fdb59bf336c297df2643120358a))

# [4.3.0](https://gitlab.com/to-be-continuous/golang/compare/4.2.0...4.3.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([3724df5](https://gitlab.com/to-be-continuous/golang/commit/3724df5332997eeb5b39887fe60c29a708917987))

# [4.2.0](https://gitlab.com/to-be-continuous/golang/compare/4.1.2...4.2.0) (2023-03-29)


### Features

* add govulncheck ([bc4a87d](https://gitlab.com/to-be-continuous/golang/commit/bc4a87d65f1ebadd3febbd9f47774be98945a1ff))

## [4.1.2](https://gitlab.com/to-be-continuous/golang/compare/4.1.1...4.1.2) (2023-03-28)


### Bug Fixes

* **sbom:** add CycloneDX report ([58f0ed6](https://gitlab.com/to-be-continuous/golang/commit/58f0ed64ac850dacbd45a24acca50b6d74e63318))

## [4.1.1](https://gitlab.com/to-be-continuous/golang/compare/4.1.0...4.1.1) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([07f8445](https://gitlab.com/to-be-continuous/golang/commit/07f8445327e782f39d5f42625553a27d88b83480))

# [4.1.0](https://gitlab.com/to-be-continuous/golang/compare/4.0.2...4.1.0) (2022-12-15)


### Features

* add a job generating software bill of materials ([97dda2f](https://gitlab.com/to-be-continuous/golang/commit/97dda2f4672f58e178acf14b5d8572d4b4ac79cd))

## [4.0.2](https://gitlab.com/to-be-continuous/golang/compare/4.0.1...4.0.2) (2022-12-14)


### Bug Fixes

* resolve build mode based on package main appearance ([b08bf37](https://gitlab.com/to-be-continuous/golang/commit/b08bf3716f7fe8c80425cdea53821f30e2ce3509))

## [4.0.1](https://gitlab.com/to-be-continuous/golang/compare/4.0.0...4.0.1) (2022-12-02)


### Bug Fixes

* go-build failing for library without main.go ([d33b1bc](https://gitlab.com/to-be-continuous/golang/commit/d33b1bcb76b52c00989c7e822e398d204ad33b83))

# [4.0.0](https://gitlab.com/to-be-continuous/golang/compare/3.2.1...4.0.0) (2022-10-04)


### Features

* normalize reports ([823a5aa](https://gitlab.com/to-be-continuous/golang/commit/823a5aa75974e02807204f23d7ab819c559e9e1a))


### BREAKING CHANGES

* generated reports have changed (see doc). It is a breaking change if you're using SonarQube.

## [3.2.1](https://gitlab.com/to-be-continuous/golang/compare/3.2.0...3.2.1) (2022-09-03)


### Bug Fixes

* add a check of git command ([31141e5](https://gitlab.com/to-be-continuous/golang/commit/31141e529431b7e5ad752d8d801f294303ba6ae0))

# [3.2.0](https://gitlab.com/to-be-continuous/golang/compare/3.1.0...3.2.0) (2022-08-12)


### Features

* **build:** add variable for go linker flags ([62c3e54](https://gitlab.com/to-be-continuous/golang/commit/62c3e54655e23bf447027d266f0845f2247c7a8f))

# [3.1.0](https://gitlab.com/to-be-continuous/golang/compare/3.0.0...3.1.0) (2022-08-09)


### Bug Fixes

* **cilint:** produce all reports at once ([7834529](https://gitlab.com/to-be-continuous/golang/commit/783452997f13fd628444676c54fbbc94eb47bfd6))
* **lint:** always produce Sonar report even when lint fails ([a5ba502](https://gitlab.com/to-be-continuous/golang/commit/a5ba502ce293a9466c42402af0d6c0b0228efc72))
* **test:** always produce reports, even when tests fail ([a4fa428](https://gitlab.com/to-be-continuous/golang/commit/a4fa42804a0b8135024b6d25d10017b83e51312c))
* **test:** shellcheck ([b06df48](https://gitlab.com/to-be-continuous/golang/commit/b06df4867282ab100c4393ef94f21d0022b5e199))


### Features

* **cilint:** add codeclimate report ([0336474](https://gitlab.com/to-be-continuous/golang/commit/0336474b7a06052e3825a3d47d13e507d37865bc))

# [3.0.0](https://gitlab.com/to-be-continuous/golang/compare/2.3.0...3.0.0) (2022-08-05)


### Features

* adaptive pipeline ([65c6cdd](https://gitlab.com/to-be-continuous/golang/commit/65c6cdd782114f01347b1b34447cc3cecac8fec1))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [2.3.0](https://gitlab.com/to-be-continuous/golang/compare/2.2.0...2.3.0) (2022-07-24)


### Features

* be able to use private go modules ([de14612](https://gitlab.com/to-be-continuous/golang/commit/de14612b3a66f9d765ec85ad89634c6cbf908b40))

# [2.2.0](https://gitlab.com/to-be-continuous/golang/compare/2.1.3...2.2.0) (2022-05-01)


### Features

* configurable tracking image ([53e20b2](https://gitlab.com/to-be-continuous/golang/commit/53e20b285200d8b7bc7bd30dfc9a24fa3c0f4897))

## [2.1.3](https://gitlab.com/to-be-continuous/golang/compare/2.1.2...2.1.3) (2022-02-25)


### Bug Fixes

* **artifacts:** always publish test artifacts ([8e0a1c0](https://gitlab.com/to-be-continuous/golang/commit/8e0a1c0b455525c7515aa4b632baa5426a48285d))

## [2.1.2](https://gitlab.com/to-be-continuous/golang/compare/2.1.1...2.1.2) (2022-01-07)


### Bug Fixes

* Suffix version for go install command ([40065b4](https://gitlab.com/to-be-continuous/golang/commit/40065b4a7f8fd699a36c160d73634359f2dd453f))

## [2.1.1](https://gitlab.com/to-be-continuous/golang/compare/2.1.0...2.1.1) (2022-01-06)


### Bug Fixes

* use go install instead of deprecated go get ([91a5c14](https://gitlab.com/to-be-continuous/golang/commit/91a5c14c8ba6bb5bff0e484a4507297b2292b8e8))

# [2.1.0](https://gitlab.com/to-be-continuous/golang/compare/2.0.3...2.1.0) (2021-11-29)


### Features

* Add GO_PROJECT_DIR variable ([ce66c08](https://gitlab.com/to-be-continuous/golang/commit/ce66c088f281363fa12989ab8a6a8563d00c3196))

## [2.0.3](https://gitlab.com/to-be-continuous/golang/compare/2.0.2...2.0.3) (2021-11-27)


### Bug Fixes

* add missing test packages for sonar test run ([89f5881](https://gitlab.com/to-be-continuous/golang/commit/89f58815118b07c2f945177969910f9c426a7b08))

## [2.0.2](https://gitlab.com/to-be-continuous/golang/compare/2.0.1...2.0.2) (2021-11-26)


### Bug Fixes

* go test packages var typo ([041c464](https://gitlab.com/to-be-continuous/golang/commit/041c4644a7b14969ad592c096e1a68fd8bc01508))

## [2.0.1](https://gitlab.com/to-be-continuous/golang/compare/2.0.0...2.0.1) (2021-11-26)


### Bug Fixes

* sonar test report ([e0da931](https://gitlab.com/to-be-continuous/golang/commit/e0da93146900b1c2bb0270dbf5d24a195c7b9c9c))

# [2.0.0](https://gitlab.com/to-be-continuous/golang/compare/1.3.0...2.0.0) (2021-11-23)


### Features

* **build:** build target platform can be specified ([b068c40](https://gitlab.com/to-be-continuous/golang/commit/b068c40e5095bb69eb743832fb10e1cdb04d826a))


### BREAKING CHANGES

* **build:** changed `GO_BUILD_ARGS` to `GO_BUILD_FLAGS` variable and `GO_TEST_ARGS` to `GO_TEST_FLAGS` variable. See doc.

# [1.3.0](https://gitlab.com/to-be-continuous/golang/compare/1.2.2...1.3.0) (2021-11-21)


### Features

* make lint job auto on feature branches ([88324d9](https://gitlab.com/to-be-continuous/golang/commit/88324d9d60a60abaf50a62dc4ce0e4b93e20d566))

## [1.2.2](https://gitlab.com/to-be-continuous/golang/compare/1.2.1...1.2.2) (2021-10-07)


### Bug Fixes

* use master or main for production env ([758162a](https://gitlab.com/to-be-continuous/golang/commit/758162a50c91d21eda516f5c9930980b410ab7fe))

## [1.2.1](https://gitlab.com/to-be-continuous/golang/compare/1.2.0...1.2.1) (2021-09-03)

### Bug Fixes

* Change boolean variable behaviour ([835c547](https://gitlab.com/to-be-continuous/golang/commit/835c5477a4d8e596b344f0806e918c1b1bc4d67c))

## [1.2.0](https://gitlab.com/to-be-continuous/golang/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([368e5a2](https://gitlab.com/to-be-continuous/golang/commit/368e5a2fbbe61bbe5bffeaa6c4bfcea101f8c47b))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/golang/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([cb75be4](https://gitlab.com/Orange-OpenSource/tbc/golang/commit/cb75be4aebdda6c510f8ea2af76308078db692d9))

## 1.0.0 (2021-05-06)

### Features

* initial release ([5518d12](https://gitlab.com/Orange-OpenSource/tbc/golang/commit/5518d126a81b2b8feb529b958868942f4b1bf900))
